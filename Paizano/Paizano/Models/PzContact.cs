﻿using Plugin.Contacts.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
    public class PzContact : Contact
    {
        public string InitialChar
        {
            get { return base.DisplayName.Substring(0,1).ToUpper(); }
        }
        public string MobileNums
        {
            //get { return String.Join(",", base.Phones.Select(p => p.Number).ToList()); }
            get { return base.Phones.Select(p => p.Number).ToList().FirstOrDefault(); }
        }
    }
}
