﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
   public class LedgerDetail
    {
        public string SNo { get; set; }
        public string SendType { get; set; }
        public string Type { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public int TotalSend { get; set; }
        public int TotalReceive { get; set; }


    }
    public class UserTransactionData
    {
        public string ID { get; set; }
        public string ToMobile { get; set; }
        public string FromMobile { get; set; }
        public string Type { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
    }

    public class UserTransactionResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
        public string Piclink { get; set; }
        public string Mobile { get; set; }
        public List<UserTransactionData> data { get; set; }
    }
}
