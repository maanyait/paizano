﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
    public class Signup
    {
        public string username { get; set; }
        public string password { get; set; }
        public string msisdn { get; set; }
        public string msg { get; set; }
        public string source { get; set; }
        public string otplen { get; set; }
        public string tagname { get; set; }
        public string exptime { get; set; }
    }

    
    public class Country
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string Currency { get; set; }
        public string DefaultLanguage { get; set; }

    }
    public class CountryDetailResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public List<Country> data { get; set; }
    }


    public class State
    {
        public int ID { get; set; }
        public string StateName { get; set; }
        public int CountryID { get; set; }
    }



    public class City
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int CountryID { get; set; }
        public int StateID { get; set; }

    }


    public class StateRequest
    {
        public int CountryID { get; set; }
        public string device { get; set; }
    }

    public class CityRequest
    {
        public int CountryID { get; set; }
        public int StateID { get; set; }
        public string device { get; set; }
    }


   

    public class CityDetailResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public List<City> data { get; set; }
    }
    public class StateDetailResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public List<State> data { get; set; }
    }

    public class TransactionType
    {
        public int TransactionTypeID { get; set; }
        public string TransactionTypeName { get; set; }

    }

    public class TransactionTypeResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<TransactionType> data { get; set; }
    }
    public class TransactionResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string data { get; set; }
    }
}
