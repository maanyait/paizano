﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
    public class TransactionModel
    {
        
        public int FromUser { get; set; }
        public string ToMobile { get; set; }
        public string ToUserDisplayName { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
        public string RequestType { get; set; }
        public int TransactionTypeID { get; set; }
        public string TransactionTypeName { get; set; }
        public int Addedby { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime CreatedDate { get; set;}
        public string Isinitiated { get; set; }
        public string IsAccepted { get; set; }
        public string Color { get; set; }
        public string Status { get; set; }
        public string ShowData { get; set; }
        public long ID { get; set; }
    }
    public class TransHistoryResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<TransactionModel> data { get; set; }
    }

    public class TransResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string data { get; set; }
    }
}
