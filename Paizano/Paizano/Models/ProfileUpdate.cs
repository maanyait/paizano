﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
   public class ProfileUpdate
    {
        public int UserID { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string dob { get; set; }
        public string Photo { get; set; }
        public string Imagestring { get; set; }
    }

    public class GetProfileResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public ProfileUpdate data { get; set; }
    }

    public class ProfileResponse
    {
        public string status { get; set; }
        public string Message { get; set; }
        public string data { get; set; }
    }
}
