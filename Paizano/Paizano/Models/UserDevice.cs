﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paizano.Models
{
    public class UserDevice
    {
        public string DeviceID { get; set; }
        public int UserID { get; set; }
    }
}
