﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LedgerPage : ContentPage
    {
        private LedgerPageViewModel _LedgerPageViewModel;
        public LedgerPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Ledger";
            _LedgerPageViewModel = new LedgerPageViewModel();
            BindingContext = _LedgerPageViewModel;
        }



        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            if (e.Item == null)
            {
                return;
            }
               // do something with e.SelectedItem
               ((ListView)sender).SelectedItem = null;
            // de-select the row after ripple effect
        }
    }
}
