﻿using Paizano.ViewModels;
using Plugin.Contacts;
using Plugin.Contacts.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactPage : ContentPage
    {
        private ContactViewModel _contactVM;
        public ContactPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Contact Details";
            _contactVM = new ContactViewModel(Navigation);
            BindingContext = _contactVM;
        }
        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            if (e.Item == null)
            {
                return;
            }
            // do something with e.SelectedItem
            ((ListView)sender).SelectedItem = null;
            // de-select the row after ripple effect
        }

        private void srch_contact_TextChanged(object sender, TextChangedEventArgs e)
        {
            var searchStr = (sender as SearchBar);
            if (!string.IsNullOrEmpty(searchStr.Text))
            {
                _contactVM.SearchText = searchStr.Text;
                _contactVM.ExecuteOnSearch();
            }
            else
                _contactVM.LoadContacts();
        }
    }
}
