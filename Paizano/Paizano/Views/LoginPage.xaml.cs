﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
using Acr.UserDialogs;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private LoginViewModel _LoginViewModel;
        public LoginPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _LoginViewModel = new LoginViewModel(Navigation);
            BindingContext = _LoginViewModel;
        }

        private void btnsignup_Clicked(object sender, EventArgs e)
        {
            _LoginViewModel.ExecuteOnsignup();
        }

        private void btnlogin_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(logintxt.Text) && !string.IsNullOrEmpty(passwordtxt.Text))
            {
                UserDialogs.Instance.Loading("Please Wait...");
                _LoginViewModel.ExecuteOnLogin();
            }
            else
            {
                UserDialogs.Instance.Alert("Please Enter Login ID and Password", "Message", "Ok");
            }
        }
    }
}
