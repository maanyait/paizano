﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paizano.Common.Constants;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuNavigationPage : MasterDetailPage
    {
        public MenuNavigationPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            Detail = new DashboardPage();
            Cache.MasterPage = this;
        }
    }
}
