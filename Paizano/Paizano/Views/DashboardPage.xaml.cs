﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
using Paizano.Models;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardPage : ContentPage
    {
        private DashboardPageViewModel _dbviewmodel;
        public DashboardPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            _dbviewmodel = new DashboardPageViewModel();
            BindingContext = _dbviewmodel;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            TransactionModel trns = (TransactionModel)e.Item;

            if (trns.Status == "Pending")
            {
                _dbviewmodel.OnItemTapped(trns);
            }
            if (e.Item == null)
            {
                return;
            }
            // do something with e.SelectedItem
            ((ListView)sender).SelectedItem = null;
            // de-select the row after ripple effect
        }
    }
}
