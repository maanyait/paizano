﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OTPVerificationPage : ContentPage
    {
        private OTPVerificationViewModel _OTPVerificationViewModel;
        public OTPVerificationPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "OTP";
           
            _OTPVerificationViewModel = new OTPVerificationViewModel();
            BindingContext = _OTPVerificationViewModel;
        }

        private void btnverification_Clicked(object sender, EventArgs e)
        {
           
                _OTPVerificationViewModel.ExecuteOnVerify(txtotp.Text);
            
        }
    }
}
