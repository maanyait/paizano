﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgetPage : ContentPage
    {
        private ForgetViewModel _ForgetViewModel;
        public ForgetPage()
        {
            InitializeComponent();
            _ForgetViewModel = new ForgetViewModel();
            BindingContext = _ForgetViewModel;
        }

        
    }
}
