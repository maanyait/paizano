﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paizano.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.Models;
using Paizano.Helpers;
using Acr.UserDialogs;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        private RegistrationViewModel _RegistrationViewModel;
        public RegistrationPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Personal Info";
            _RegistrationViewModel = new RegistrationViewModel();
            BindingContext = _RegistrationViewModel;
        }

        private void btnsave_Clicked(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtemail.Text))
            {
                UserDialogs.Instance.Alert("Please enter email.", "Message", "Ok");
                return;
            }
            if (string.IsNullOrEmpty(txtpassword.Text))
            {
                UserDialogs.Instance.Alert("Please enter password.", "Message", "Ok");
                return;
            }
            if (string.IsNullOrEmpty(txtreenter.Text))
            {
                UserDialogs.Instance.Alert("Please re-enter password.", "Message", "Ok");
                return;
            }
            if (drp_questions.SelectedItem == null)
            {
                UserDialogs.Instance.Alert("Please select security question.", "Message", "Ok");
                return;
            }
            if (string.IsNullOrEmpty(txtanswer.Text))
            {
                UserDialogs.Instance.Alert("Please enter security answer.", "Message", "Ok");
                return;
            }

            if (txtpassword.Text != txtreenter.Text)
            {
                UserDialogs.Instance.Alert("Confirm Password do not match with Password.", "Message", "Ok");
                return;
            }
            UserAndSecurity user = new UserAndSecurity();
            user.email = txtemail.Text;
            user.Password = txtpassword.Text;
            user.Answer = txtanswer.Text;
            user.device = Settings.PZN_DeviceID;
            user.flag = 3;
            user.Mobile = Settings.PZN_Mobile;
            user.QuestionID = ((SecurityQuestion)drp_questions.SelectedItem).ID;
            user.Country = Settings.PZN_Country;
            user.City = Settings.PZN_City;
            _RegistrationViewModel.ExecuteOnSave(user);
        }
    }
}
