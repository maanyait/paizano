﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paizano.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FavContactPage : ContentPage
    {
        private FavContactDetailViewModel _contactVM;
        public FavContactPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Favorite Contact Details";
            _contactVM = new FavContactDetailViewModel();
            BindingContext = _contactVM;
        }


        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            if (e.Item == null)
            {
                return;
            }
               // do something with e.SelectedItem
               ((ListView)sender).SelectedItem = null;
            // de-select the row after ripple effect
        }

       
    }
}
