﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
using Paizano.Models;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        private MenuViewModel _MenuViewModel;
        public MenuPage()
        {
            InitializeComponent();
             NavigationPage.SetHasNavigationBar(this, false);
            _MenuViewModel = new MenuViewModel();
            BindingContext = _MenuViewModel;
        }

        //private void listmenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        //{
        //    var item = (Menu)e.SelectedItem;
        //    Type page = item.TargetType;

        //    _MenuViewModel.ExecuteOnMenuclick(page);
        //}
        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            if (e.Item == null)
            {
                return;
            }
            // do something with e.SelectedItem
            ((ListView)sender).SelectedItem = null;
            // de-select the row after ripple effect
        }
    }
}
