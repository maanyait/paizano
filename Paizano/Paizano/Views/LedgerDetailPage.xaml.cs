﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paizano.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Paizano.ViewModels;
namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LedgerDetailPage : ContentPage
    {
        private LedgerDetailsViewModel _ldviewmodel;
        public LedgerDetailPage(PzContact selectedcontact)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Ledger Detail";
            _ldviewmodel = new LedgerDetailsViewModel(selectedcontact);
            BindingContext = _ldviewmodel;
        }
    }
}
