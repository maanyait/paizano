﻿using Paizano.Models;
using Paizano.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Paizano.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SendMoneyPage : ContentPage
    {
        private SendMoneyViewModel _sendMoneyVM;
        public SendMoneyPage(PzContact selectedcontact)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            this.Title = "Send";
            _sendMoneyVM = new SendMoneyViewModel(Navigation, selectedcontact);
            BindingContext = _sendMoneyVM;
        }
    }
}
