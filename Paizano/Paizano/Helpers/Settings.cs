// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Paizano.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;

        private const string mobileno = "Monile No";
        private static readonly string Defaultmobileno = string.Empty;

        private const string country = "Country Name";
        private static readonly string Defaultcountry = string.Empty;

        private const string state = "State Name";
        private static readonly string Defaultstate = string.Empty;

        private const string City = "City Name";
        private static readonly string DefaultCity = string.Empty;

        private const string Piclink = "Piclink";
        private static readonly string DefaultPiclink = string.Empty;

        private const string DisplayName = "Display Name";
        private static readonly string DefaultDisplayName = string.Empty;

        private const string UserID = "UserID";
        private static readonly string DefaultUserID = string.Empty;

        private const string Password = "Password";
        private static readonly string DefaultPassword = string.Empty;

        private const string DeviceID = "DeviceID";
        private static readonly string DefaultDeviceID = string.Empty;

        private const string Currency = "Currency";
        private static readonly string DefaultCurrency = string.Empty;


        private const string OneSignalID = "OneSignal";
        private static readonly string DefaultOneSignalID = string.Empty;


        private const string accesstoken = "aCCESSTOKEN";
        private static readonly string DefaultAccessToken = string.Empty;


        private const string renewtoken = "renewtoken";
        private static readonly string DefaultRenewToken= string.Empty;


        #endregion

        public static string AccessTokenSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(accesstoken, DefaultAccessToken);
            }
            set
            {
                AppSettings.AddOrUpdateValue(accesstoken, value);
            }
        }

        public static string RenewalTokenSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(renewtoken, DefaultRenewToken);
            }
            set
            {
                AppSettings.AddOrUpdateValue(renewtoken, value);
            }
        }


        public static string PZN_OneSignalID
        {
            get
            {
                return AppSettings.GetValueOrDefault(OneSignalID, DefaultOneSignalID);
            }
            set
            {
                AppSettings.AddOrUpdateValue(OneSignalID, value);
            }
        }
        public static string PZN_Currency
        {
            get
            {
                return AppSettings.GetValueOrDefault(Currency, DefaultCurrency);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Currency, value);
            }
        }

        public static string PZN_Mobile
        {
            get
            {
                return AppSettings.GetValueOrDefault(mobileno, Defaultmobileno);
            }
            set
            {
                AppSettings.AddOrUpdateValue(mobileno, value);
            }
        }


        public static string PZN_Country
        {
            get
            {
                return AppSettings.GetValueOrDefault(country, Defaultcountry);
            }
            set
            {
                AppSettings.AddOrUpdateValue(country, value);
            }
        }

        public static string PZN_State
        {
            get
            {
                return AppSettings.GetValueOrDefault(state, Defaultstate);
            }
            set
            {
                AppSettings.AddOrUpdateValue(state, value);
            }
        }


        public static string PZN_City
        {
            get
            {
                return AppSettings.GetValueOrDefault(City, DefaultCity);
            }
            set
            {
                AppSettings.AddOrUpdateValue(City, value);
            }
        }

        public static string PZN_Piclink
        {
            get
            {
                return AppSettings.GetValueOrDefault(Piclink, DefaultPiclink);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Piclink, value);
            }
        }

        public static string PZN_DisplayName
        {
            get
            {
                return AppSettings.GetValueOrDefault(DisplayName, DefaultDisplayName);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DisplayName, value);
            }
        }

        public static string PZN_UserID
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserID, DefaultUserID);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserID, value);
            }
        }

        public static string PZN_Password
        {
            get
            {
                return AppSettings.GetValueOrDefault(Password, DefaultPassword);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Password, value);
            }
        }

        public static string PZN_DeviceID
        {
            get
            {
                return AppSettings.GetValueOrDefault(DeviceID, DefaultDeviceID);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DeviceID, value);
            }
        }

    }
}