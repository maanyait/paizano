﻿using Paizano.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Paizano.CustomViews
{
    public partial class MasterHeader : ContentView
    {
        public MasterHeader()
        {
            InitializeComponent();
        }
        private void OnMenuTapped(object sender, EventArgs e)
        {
            Cache.MasterPage.IsPresented = true;
        }
    }
}
