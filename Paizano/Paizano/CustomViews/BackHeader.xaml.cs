﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Paizano.CustomViews
{
    public partial class BackHeader : ContentView
    {
        public BackHeader()
        {
            InitializeComponent();
        }
        private void OnBackTapped(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
