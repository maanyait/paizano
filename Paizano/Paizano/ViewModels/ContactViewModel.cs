﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Paizano.Helpers;
using Paizano.Models;
using Plugin.Contacts;
using Plugin.Contacts.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
    public class ContactViewModel : BaseViewModel
    {
        private INavigation _navigation;
        public ContactViewModel(INavigation navigation)
        {
            this._navigation = navigation;
            PageHeaderText = "Contact Details";
            PageSubHeaderText = "";
            SearchContact = new Command(ExecuteOnSearch);
            LoadContacts();
        }
        #region Properties

        public List<PzContact> _pzcontact;
        public List<PzContact> PZcontacts
        {
            get { return _pzcontact; }
            set { _pzcontact = value; OnPropertyChanged("PZcontacts"); }
        }

        private string _searchtext = string.Empty;
        public string SearchText
        {
            get { return _searchtext; }
            set { _searchtext = value; OnPropertyChanged(); }
        }
        private PzContact _selectedcontact;
        public PzContact SelectedContact
        {
            get { return _selectedcontact; }
            set
            {
                _selectedcontact = value;
                GoToSeletedContact();
            }
        }

        #endregion
        public Command SearchContact { get; set; }

        public async void ExecuteOnSearch()
        {
            var contacts = await PzFileSystem.ReadAllTextAsync("pzcontacts.txt");
            var tempcontacts = JsonConvert.DeserializeObject<List<PzContact>>(contacts);

            PZcontacts = tempcontacts.Where(cont => !string.IsNullOrWhiteSpace(cont.LastName) &&
            (cont.DisplayName.ToLower().Contains(SearchText)) ||
            (cont.MobileNums.Contains(SearchText))
            ).OrderBy(ol => ol.DisplayName).ToList();

        }

        public async void LoadContacts()
        {
            try
            {
                if (!await PzFileSystem.IsFileExistAsync("pzcontacts.txt"))
                {
                    if (await CrossContacts.Current.RequestPermission())
                    {
                        List<Contact> contacts = null;
                        CrossContacts.Current.PreferContactAggregation = false;//recommended
                        await Task.Run(() => //run in background
                        {
                            UserDialogs.Instance.ShowLoading("");
                            if (CrossContacts.Current.Contacts == null)
                                return;
                            contacts = CrossContacts.Current.Contacts.ToList();
                            contacts = contacts.Where(cont => !string.IsNullOrWhiteSpace(cont.LastName) && cont.Phones.Count > 0).OrderBy(ol => ol.DisplayName).ToList();
                            PZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(JsonConvert.SerializeObject(contacts));
                            UserDialogs.Instance.HideLoading();
                        });
                        await PzFileSystem.CreateFile("pzcontacts.txt");
                        await PzFileSystem.WriteTextAllAsync("pzcontacts.txt", JsonConvert.SerializeObject(PZcontacts));
                    }
                }
                else
                {
                    var contacts = await PzFileSystem.ReadAllTextAsync("pzcontacts.txt");
                    PZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(contacts);
                }
            }
            catch
            {

            }
        }

        private async void GoToSeletedContact()
        {
            if (SelectedContact == null)
            {
                return;
            }
            UserDialogs.Instance.ShowLoading("Loading...");
            await _navigation.PushAsync(new Views.SendMoneyPage(SelectedContact));
        }
    }
}
