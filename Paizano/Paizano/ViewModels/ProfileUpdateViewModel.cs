﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paizano.Models;
using Xamarin.Forms;
using Paizano.Helpers;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Acr.UserDialogs;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;

namespace Paizano.ViewModels
{
  public class ProfileUpdateViewModel:BaseViewModel
    {
        #region Constructor
        public ProfileUpdateViewModel()
        {
            PfUpdate = new ProfileUpdate();
            OnPagePreparation();

            Save = new Command(ExecuteOnSave);
            Cancel = new Command(ExecuteOnCancel);
            UploadImg = new Command(ExecuteOnUpload);
        }

        #endregion

        #region Property

        private string first { get; set; }
        public string First
        {
            get { return first; }
            set { first = value; OnPropertyChanged("First"); }
        }
        private string last { get; set; }
        public string Last
        {
            get { return last; }
            set { last = value; OnPropertyChanged("Last"); }
        }
        private string email { get; set; }
        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }
        private string photo { get; set; }
        public string Photo
        {
            get { return photo; }
            set { photo = value; OnPropertyChanged("Photo"); }
        }

        private string imageString { get; set; }
        public string ImageString
        {
            get { return imageString; }
            set { imageString = value; OnPropertyChanged("ImageString"); }
        }
        private ProfileUpdate pfupdate;
        public ProfileUpdate PfUpdate
        {
            get { return pfupdate; }
            set { pfupdate = value; OnPropertyChanged("PfUpdate"); }

        }

        #endregion

        #region Command

        public Command Save { get; set; }
        public Command Cancel { get; set; }
        public Command UploadImg { get; set; }

        #endregion

        #region Method

        public async void OnPagePreparation()
        {
            try
            {
                
                UserDialogs.Instance.Loading("Please Wait");
                ProfileUpdate objinfo = new ProfileUpdate();
                objinfo.UserID =Convert.ToInt32(Settings.PZN_UserID);
                string profilejson = JsonConvert.SerializeObject(objinfo);
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_GetProfile, Settings.AccessTokenSettings);
                var response =await apicall.Post<GetProfileResponse>(profilejson);
                if(response!=null)
                {
                    if(response?.status != null &&  response.status == "true")
                    {
                        First = response.data.firstname;
                        Last = response.data.lastname;
                        Email= response.data.email;
                        Photo = ApiUrls.BaseUrlWeb +response.data.Photo;
                        Settings.PZN_Piclink = response.data.Photo;
                        //PfUpdate.UserID = response.data.UserID;
                    }
                }
                UserDialogs.Instance.Loading().Hide();
            }
            catch (Exception ee)
            {
                await App.Current.MainPage.DisplayAlert("Error",ee.Message,"OK");
                UserDialogs.Instance.Loading().Hide();
               
            }
        }

        private async void ExecuteOnSave()
        {
            try
            {
                UserDialogs.Instance.Loading("Please Wait");
                PfUpdate.firstname = First;
                PfUpdate.lastname = Last;
                PfUpdate.email = Email;
                pfupdate.Imagestring = "";
                
                PfUpdate.UserID =Convert.ToInt32( Settings.PZN_UserID);
                
                var profilejson = JsonConvert.SerializeObject(PfUpdate);
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_UpdateProfile, Settings.AccessTokenSettings);
                var response = await apicall.Post<ProfileResponse>(profilejson);
                if(response!=null)
                {
                    if(response?.status!=null && response.status=="true")
                    {
                        await App.Current.MainPage.DisplayAlert("Message","Profile Updated Successfully.","OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.ProfileUpdatePage());
                    }else
                    {
                        await App.Current.MainPage.DisplayAlert("Message", "Profile not Updated.", "OK");
                    }
                }


                UserDialogs.Instance.Loading().Hide();
            }
            catch (Exception ee)
            {

                UserDialogs.Instance.Loading().Hide();
                await App.Current.MainPage.DisplayAlert("Error", ee.Message, "OK");
            }
        }



        private async void ExecuteOnUpload()
        {
            try
            {
                try
                {
                    FileData fileData = new FileData();

                    fileData = await CrossFilePicker.Current.PickFile();
                    byte[] data = fileData.DataArray;
                    ImageString = Convert.ToBase64String(data);
                    
                }
                catch
                {
                    ImageString = "";
                }

                UserDialogs.Instance.Loading("Please Wait");
                PfUpdate.firstname = First;
                PfUpdate.lastname = Last;
                PfUpdate.email = Email;
                if (!string.IsNullOrEmpty(ImageString))
                {
                    PfUpdate.Photo = "";
                    pfupdate.Imagestring = ImageString;
                }
                else
                {
                    PfUpdate.Photo = Photo.Substring(35);
                    pfupdate.Imagestring = "";
                }
                PfUpdate.UserID = Convert.ToInt32(Settings.PZN_UserID);

                var profilejson = JsonConvert.SerializeObject(PfUpdate);
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_UpdateProfile, Settings.AccessTokenSettings);
                var response = await apicall.Post<ProfileResponse>(profilejson);
                if (response != null)
                {
                    if (response?.status != null && response.status == "true")
                    {
                       
                        await App.Current.MainPage.Navigation.PushAsync(new Views.ProfileUpdatePage());
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Message", "Profile Photo not Updated.", "OK");
                    }
                }


                UserDialogs.Instance.Loading().Hide();


            }
            catch {
                UserDialogs.Instance.Loading().Hide();
               
            }
        }

        private async void ExecuteOnCancel()
        {
            await App.Current.MainPage.Navigation.PushAsync(new Views.MenuNavigationPage());
        }

        #endregion



    }
    }
