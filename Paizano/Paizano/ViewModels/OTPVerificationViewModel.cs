﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
   public class OTPVerificationViewModel:BaseViewModel
    {
        public OTPVerificationViewModel()
        {
            Count = 3;
            TapResend = new Command(ExecuteOnResend);
        }

        #region Property 

        private int _Count;
        public int Count
        {
            get { return _Count; }
            set { _Count = value; OnPropertyChanged("Count"); }
        }

        private string _msg;
        public string MSG
        {
            get { return _msg; }
            set { _msg = value; OnPropertyChanged("MSG"); }
        }
        #endregion 


        #region Command

        public Command TapResend { get; set; }

        #endregion


        public async void ExecuteOnVerify(string OTP)
        {
            try
            {
              

                HttpClientHelper apicall = new HttpClientHelper(string.Format(ApiUrls.URl_VerifyOTP, Settings.PZN_Mobile, OTP), string.Empty);


                var response = await apicall.PostOTP<string>("");
                MSG = "You have " + Count + " attempt left.";
                switch (response)
                    {
                    case "101":
                        await App.Current.MainPage.Navigation.PushAsync(new Views.RegistrationPage());
                        break;

                    case "102":
                        await App.Current.MainPage.DisplayAlert("Message", "OTP is expired", "OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.SignUpPage());
                        break;

                    case "103":
                        await App.Current.MainPage.DisplayAlert("Message", "Entry for OTP not found", "OK");
                        MSG = "You have "+Count+" attempt left.";
                        break;

                    case "104":
                        await App.Current.MainPage.DisplayAlert("Message", "MSISDN not found", "OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.SignUpPage());
                        break;

                    case "1702":
                        await App.Current.MainPage.DisplayAlert("Message", "One of the parameter missing or OTP is not Numeric", "OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.SignUpPage());
                        break;

                    case "1703":
                        await App.Current.MainPage.DisplayAlert("Message", "Authentication failed", "OK");
                        MSG = "You have " + Count + " attempt left.";
                        
                        break;

                    case "1706":
                        await App.Current.MainPage.DisplayAlert("Message", "Given destination is invalid", "OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.SignUpPage());
                        break;

                    default:
                        await App.Current.MainPage.DisplayAlert("Message", "Authentication failed", "OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.SignUpPage());
                        break;

                }
                if (Count < 0)
                {
                    await App.Current.MainPage.Navigation.PushAsync(new Views.SignUpPage());
                }
                Count--;

            }
            catch (Exception)
            {
            }
           
        }
        public async void ExecuteOnResend()
        {
            try
            {
                string message = "Your OTP is %m valid for 5 minut. Do not share your OTP to any one.";
                HttpClientHelper apicall = new HttpClientHelper(string.Format(ApiUrls.URl_OTP, Settings.PZN_Mobile, message), string.Empty);


                var response = await apicall.PostOTP<string>("");
                if (response.Contains("1701"))
                {
                    MSG = "OTP Send Successfully.";
                }else
                {
                    await App.Current.MainPage.DisplayAlert("Message", "Failed ! Please try later", "OK");
                }

            }
            catch (Exception)
            {
            }

        }







    }
}
