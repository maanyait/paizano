﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;

namespace Paizano.ViewModels
{
    public class LedgerDetailsViewModel:BaseViewModel
    {
        public LedgerDetailsViewModel(PzContact contacts)
        {

            OnPagepreparation(contacts);
        }


        #region  Property

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        private string mobileno;
        public string MobileNo
        {
            get { return mobileno; }
            set { mobileno = value; OnPropertyChanged("MobileNo"); }
        }

        private string photo;
        public string Photo
        {
            get { return photo; }
            set { photo = value; OnPropertyChanged("Photo"); }
        }
        private string totalsent;
        public string TotalSent
        {
            get { return totalsent; }
            set { totalsent = value; OnPropertyChanged("TotalSent"); }
        }
        private string totalreceive;
        public string TotalReceive
        {
            get { return totalreceive; }
            set { totalreceive = value; OnPropertyChanged("TotalReceive"); }
        }
       

        private List<LedgerDetail> _LZList;
        public List<LedgerDetail> LZList
        {
            get { return _LZList; }
            set { _LZList = value; OnPropertyChanged("LZList"); }
        }

        #endregion
        public async void OnPagepreparation(PzContact pzcontact)
        {
            LZList = await GetLedgerDetail(pzcontact);
            if(LZList.Count>0)
            {
                Name ="Name: " +LZList[0].Name;
                MobileNo ="Mobile No.: "+ LZList[0].MobileNo;
                Photo = LZList[0].Photo;
                TotalSent ="Sent: "+ LZList.Where(x => x.SendType == "Sent").ToList().Count.ToString();
                TotalReceive="Receive: "+ LZList.Where(x => x.SendType == "Receive").ToList().Count.ToString();
            }
        }

        public async Task<List<LedgerDetail>> GetLedgerDetail(PzContact pzcontact)
        {
            var ledgerlist = new List<LedgerDetail>();
            try
            {
                FavoriteRequest objinfo = new FavoriteRequest();
                objinfo.MobileNo = pzcontact.MobileNums;
                objinfo.CurrentMobile = Settings.PZN_Mobile;
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_GetLedgerDetail, Settings.AccessTokenSettings);
                string json = JsonConvert.SerializeObject(objinfo);
                var response =await apicall.Post<UserTransactionResponse>(json);
                if(response!=null)
                {
                    if(response?.status!=null && response.status=="true")
                    {
                        int i = 1;
                       
                        foreach(var item in response.data)
                        {
                            LedgerDetail objdata = new LedgerDetail();
                            if(item.ToMobile==Settings.PZN_Mobile)
                            {
                                objdata.SendType = "Receive";
                                
                            }else
                            {
                                objdata.SendType = "Sent";
                              
                            }
                            objdata.Type = item.Type;
                            objdata.MobileNo = response.Mobile;
                            objdata.Name = response.Name;
                            objdata.Photo = ApiUrls.BaseUrlWeb + response.Piclink;
                            objdata.SNo = i.ToString() ;
                            objdata.Amount = string.Format("{0:0.00}",Convert.ToDouble(item.Amount)) ;
                            objdata.Date = item.Date;
                            objdata.Description = item.Description;
                            ledgerlist.Add(objdata);
                            i++;
                        }
                    }
                }

            }
            catch 
            {

            }
            return ledgerlist;
        }
        


    }



    

}
