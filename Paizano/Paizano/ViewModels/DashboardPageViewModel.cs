﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;
using Plugin.Contacts;
using Plugin.Contacts.Abstractions;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
 public   class DashboardPageViewModel:BaseViewModel
    {
        public DashboardPageViewModel()
        {
            PageHeaderText = Settings.PZN_DisplayName;
            OnPagePreparation("4");
            TapSendMoney = new Command(ExecuteOnSendMoney);
            TapReceivedMoney = new Command(ExecuteOnReceivedMoney);
        }

        private string sentMoney;
        public string SentMoney
        {
            get { return sentMoney; }
            set { sentMoney = value; OnPropertyChanged("SentMoney"); }
        }

        private string receiveMoney;
        public string ReceiveMoney
        {
            get { return receiveMoney; }
            set { receiveMoney = value; OnPropertyChanged("ReceiveMoney"); }
        }

        public List<PzContact> _pzcontact;
        public List<PzContact> PZcontacts
        {
            get { return _pzcontact; }
            set { _pzcontact = value; OnPropertyChanged("PZcontacts"); }
        }
        private List<TransactionModel> _pzhistory = new List<TransactionModel>();
        public List<TransactionModel> PZHistory
        {
            get { return _pzhistory; }
            set { _pzhistory = value; OnPropertyChanged("PZHistory"); }
        }
        

        public Command TapSendMoney { get; set; }
        public Command TapReceivedMoney { get; set; }
        public async void OnPagePreparation(string parma)
        {
            ObservableCollection<TransactionModel> sentlist = await CountSentMoney();
            SentMoney = "Sent Money(" + sentlist.Count + ")";
           
            PZHistory = await LoadTransactionHistory(parma);

            int list = PZHistory.Where(trns => (trns.IsAccepted == "0" && trns.Isinitiated == "0")||(trns.IsAccepted == "1")).ToList().Count;
            PZHistory = PZHistory.Where(trns => trns.IsAccepted == "0" && trns.Isinitiated == "1").ToList();
            ReceiveMoney = "Received Money(" + list + ")";

        }
        public async Task<List<TransactionModel>> LoadTransactionHistory(string flag)
        {
            try
            {
                
                UserDialogs.Instance.ShowLoading("Please wait..");


                if (!await PzFileSystem.IsFileExistAsync("pzcontacts.txt"))
                {
                    if (await CrossContacts.Current.RequestPermission())
                    {
                        List<Contact> contacts = null;
                        CrossContacts.Current.PreferContactAggregation = false;//recommended
                        await Task.Run(() => //run in background
                        {
                            UserDialogs.Instance.ShowLoading("");
                            if (CrossContacts.Current.Contacts == null)
                                return;
                            contacts = CrossContacts.Current.Contacts.ToList();
                            contacts = contacts.Where(cont => !string.IsNullOrWhiteSpace(cont.LastName) && cont.Phones.Count > 0).OrderBy(ol => ol.DisplayName).ToList();
                            PZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(JsonConvert.SerializeObject(contacts));
                            UserDialogs.Instance.HideLoading();
                        });
                        await PzFileSystem.CreateFile("pzcontacts.txt");
                        await PzFileSystem.WriteTextAllAsync("pzcontacts.txt", JsonConvert.SerializeObject(PZcontacts));
                    }
                }
                else
                {
                    var contacts = await PzFileSystem.ReadAllTextAsync("pzcontacts.txt");
                    PZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(contacts);
                }
               // var contacts = await PzFileSystem.ReadAllTextAsync("pzcontacts.txt");
                //var tempcontacts = JsonConvert.DeserializeObject<List<PzContact>>(contacts);
                var list = new List<TransactionModel>();
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_ReceivedHistory, Settings.AccessTokenSettings);
                string jsonstr = "{\"UserId\":" + "" + Settings.PZN_UserID + "" + ",\"flag\":" + "" + flag + "" + "}";
                var response = await apicall.Post<TransHistoryResponse>(jsonstr);
                if (response != null)
                {
                    if (response.data != null)
                    {
                        
                        foreach (var trans in response.data)
                        {
                           
                            TransactionModel trns = new TransactionModel();
                            trns.ID = trans.ID;
                            trns.TransactionTypeID = trans.TransactionTypeID;
                            trns.TransactionTypeName = trans.TransactionTypeName;
                            trns.FromUser = trans.FromUser;
                            trns.ToMobile = trans.ToMobile;
                            var cont_usr = PZcontacts.Where(cont => cont.MobileNums.Replace("(", "").Replace(")", "").Replace("-", "").Replace("+", "").Replace(" ", "").Contains(trans.ToMobile.ToString())).FirstOrDefault();
                            trns.ToUserDisplayName = cont_usr == null ? "N/A" : cont_usr.DisplayName;
                            trns.Amount = trans.Amount;
                            trns.CreatedDate = trans.CreatedDate;
                            trns.Description = trans.Description;
                            trns.RequestType = trans.RequestType;
                            trns.Addedby = trans.Addedby;
                            trns.Isinitiated = trans.Isinitiated;
                            trns.IsAccepted = trans.IsAccepted;
                            trns.AddedDate = trans.AddedDate;
                            string status = "";

                            if (trns.IsAccepted == "0" && trns.Isinitiated == "1")
                            {
                                status = "Click to Accept";
                                trns.Status = "Pending";
                                trns.Color = "Yellow";
                               
                            }
                            else if (trns.IsAccepted == "0" && trns.Isinitiated == "0")
                            {
                                trns.Status = "Rejected";
                                trns.Color = "Red";
                            }
                            else if (trns.IsAccepted == "1")
                            {
                                trns.Status = "Accepted";
                                trns.Color = "Green";
                            }
                            trns.ShowData = trans.Amount.ToString() + ". INR sent by " + trns.ToUserDisplayName + " (" + trns.ToMobile + ") on " + trns.AddedDate + " for " + trns.TransactionTypeName + ". (" + trans.Description + "?)" + status;

                            list.Add(trns);
                        }
                    }
                }
                UserDialogs.Instance.HideLoading();
                
                return list;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(ex.Message,"Error","OK");
                return null;
            }
        }

        public async void OnItemTapped(TransactionModel trns)
        {
            try
            {
                var answer = await App.Current.MainPage.DisplayActionSheet("Are you sure want to Accept? ", "Cancel", null, "Accept", "Reject");
                string status = "0", initiate = "0";
                if (answer == "Accept")
                {
                    initiate = "1";
                    status = "1";
                }
                else if (answer == "Reject")
                {
                    initiate = "1";
                    status = "0";
                }
                else
                {
                    initiate = "0";
                    status = "0";
                }
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_AcceptRejectTransaction, Settings.AccessTokenSettings);
                string jsonstr = "{\"ID\":" + "" + trns.ID + "" + ",\"Isinitiated\":" + "" + initiate + "" + ",\"IsAccepted\":" + "" + status + "" + "}";
                var response = await apicall.Post<TransResponse>(jsonstr);
                if (response != null)
                {
                    if (response.Status != null && response.Status == "true")
                    {
                        await App.Current.MainPage.Navigation.PushAsync(new Views.ReceivedHistoryPage());
                    }
                }
            }
            catch { }
        }

        public async Task<ObservableCollection<TransactionModel>> CountSentMoney()
        {
            try
            {
                int flag = 4;
                UserDialogs.Instance.ShowLoading("Please wait..");
                var contacts = await PzFileSystem.ReadAllTextAsync("pzcontacts.txt");
                var tempcontacts = JsonConvert.DeserializeObject<List<PzContact>>(contacts);
                var list = new ObservableCollection<TransactionModel>();
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_TransactionHistory, Settings.AccessTokenSettings);
                string jsonstr = "{\"UserId\":" + "" + Settings.PZN_UserID + "" + ",\"flag\":" + "" + flag + "" + "}";
                var response = await apicall.Post<TransHistoryResponse>(jsonstr);
                if (response != null)
                {
                    if (response.data != null)
                    {
                        foreach (var trans in response.data)
                        {
                            TransactionModel trns = new TransactionModel();
                            trns.TransactionTypeID = trans.TransactionTypeID;
                           list.Add(trns);
                        }
                    }
                }
                UserDialogs.Instance.HideLoading();
                return list;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(ex.Message, "Error", "Ok");
                return null;
            }

         
        }


        public async void ExecuteOnSendMoney()
        {
            await App.Current.MainPage.Navigation.PushAsync(new Views.SendHistoryPage());
        }
        public async void ExecuteOnReceivedMoney()
        {
            await App.Current.MainPage.Navigation.PushAsync(new Views.ReceivedHistoryPage());
        }
    }
}
