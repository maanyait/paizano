﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Paizano.Common.Constants;
using Xamarin.Forms;
using Paizano.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Acr.UserDialogs;
using Paizano.Helpers;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Paizano.ViewModels
{
    public class SignUpViewModel : BaseViewModel
    {
        public SignUpViewModel()
        {
            OnPagePrepration();
        }

        private ObservableCollection<Country> _Country = new ObservableCollection<Country>();
        public ObservableCollection<Country> Getcountrylist
        {
            get { return _Country; }
            set { _Country = value; OnPropertyChanged("Getcountrylist"); }
        }

        private ObservableCollection<State> _State = new ObservableCollection<State>();
        public ObservableCollection<State> Getstatelist
        {
            get { return _State; }
            set { _State = value; OnPropertyChanged("Getstatelist"); }
        }

        private ObservableCollection<City> _City = new ObservableCollection<City>();
        public ObservableCollection<City> Getcitylist
        {
            get { return _City; }
            set { _City = value; OnPropertyChanged("Getcitylist"); }
        }

        private async void OnPagePrepration()
        {
            Getcountrylist = await GetCountryListbydeviceID(Settings.PZN_DeviceID);
        }


        private string statusMessage;
        public string StatusMessage
        {
            get { return statusMessage; }
            set { statusMessage = value; OnPropertyChanged(); }
        }

        private bool isStatusVisible;
        public bool IsStatusVisible
        {
            get { return isStatusVisible; }
            set { isStatusVisible = value; OnPropertyChanged(); }
        }

        private async Task<ObservableCollection<Country>> GetCountryListbydeviceID(string device)
        {
            try
            {
                UserDialogs.Instance.Loading("please wait..");
                var country_list = new ObservableCollection<Country>();
                CountryDetailResponse countryDetailResponse = new CountryDetailResponse();
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Get_countrylist, string.Empty);
                UserDevice usrdev = new UserDevice();
                usrdev.DeviceID = device;
                var countryjson = JsonConvert.SerializeObject(usrdev);
                var response = await apicall.Post<CountryDetailResponse>(countryjson);
                if (response != null)
                {

                    if (response.data != null)
                    {
                        foreach (var countrydetail in response.data)
                        {
                            Country country = new Country();
                            country.CountryID = countrydetail.CountryID;
                            country.CountryCode = countrydetail.CountryCode;
                            country.CountryName = countrydetail.CountryName;
                            country.Currency = countrydetail.Currency;
                            country.DefaultLanguage = countrydetail.DefaultLanguage;
                            country_list.Add(country);
                        }

                    }
                    // countryDetailResponse.data = countrylist;
                    IsStatusVisible = true;
                    StatusMessage = "true";
                    UserDialogs.Instance.Loading().Hide();

                    DisposeObject();
                    //  UserDialogs.Instance.Loading().Hide();
                }
                else
                {
                    IsStatusVisible = true;
                    StatusMessage = "false";
                    UserDialogs.Instance.Loading().Hide();
                }
                IsRunningTasks = false;
                return country_list;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(ex.Message, "Error", "Ok");
                return null;
            }
        }
        private void DisposeObject()
        {

        }

        public async void Executestatebycountryid(StateRequest objsr)
        {
            Getstatelist = await GetStateListbycountryID(objsr);
        }


        public async void ExecuteCitybystateid(CityRequest objsr)
        {
            Getcitylist = await GetCityListbycountryID(objsr);
        }

        private async Task<ObservableCollection<State>> GetStateListbycountryID(StateRequest objsr)
        {
            try
            {
                UserDialogs.Instance.Loading("please wait..");
                var State_list = new ObservableCollection<State>();
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Get_statelist, string.Empty);
                var statejson = JsonConvert.SerializeObject(objsr);
                var response = await apicall.Post<StateDetailResponse>(statejson);
                if (response != null)
                {

                    if (response.data != null)
                    {
                        foreach (var statedetail in response.data)
                        {
                            State state = new State();
                            state.CountryID = statedetail.CountryID;
                            state.ID = statedetail.ID;
                            state.StateName = statedetail.StateName;
                            State_list.Add(state);
                        }

                    }
                    // countryDetailResponse.data = countrylist;
                    IsStatusVisible = true;
                    StatusMessage = "true";
                    //  UserDialogs.Instance.Loading().Hide();

                    DisposeObject();
                    UserDialogs.Instance.Loading().Hide();
                }
                else
                {
                    IsStatusVisible = true;
                    StatusMessage = "false";
                    UserDialogs.Instance.Loading().Hide();
                }
                IsRunningTasks = false;
                return State_list;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(ex.Message, "Error", "Ok");
                return null;
            }
        }


        private async Task<ObservableCollection<City>> GetCityListbycountryID(CityRequest objcr)
        {
            try
            {
                UserDialogs.Instance.Loading("please wait..");
                var city_list = new ObservableCollection<City>();
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Get_citylist, string.Empty);
                var cityjson = JsonConvert.SerializeObject(objcr);
                var response = await apicall.Post<CityDetailResponse>(cityjson);
                if (response != null)
                {

                    if (response.data != null)
                    {
                        foreach (var citydetail in response.data)
                        {
                            City city = new City();
                            city.CountryID = citydetail.CountryID;
                            city.CityID = citydetail.CityID;
                            city.CityName = citydetail.CityName;
                            city_list.Add(city);
                        }

                    }
                    // countryDetailResponse.data = countrylist;
                    IsStatusVisible = true;
                    StatusMessage = "true";
                    //  UserDialogs.Instance.Loading().Hide();

                    DisposeObject();
                    UserDialogs.Instance.Loading().Hide();
                }
                else
                {
                    IsStatusVisible = true;
                    StatusMessage = "false";
                    UserDialogs.Instance.Loading().Hide();
                }
                IsRunningTasks = false;
                return city_list;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(ex.Message, "Error", "Ok");
                return null;
            }
        }


        public async void ExecuteOnSendOTP()
        {
            try
            {
               
                string MSG = "Your OTP is %m valid for 5 minut. Do not share your OTP to any one.";
                HttpClientHelper apicall = new HttpClientHelper(string.Format(ApiUrls.URl_OTP,Settings.PZN_Mobile,MSG), string.Empty);
              

                var response = await apicall.PostOTP<string>("");
                if (response.Contains("1701"))
                {
                    RedirectOnOTPPage();
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Message", "Failed ! Please try later", "OK");
                }

            }
            catch (Exception ee)
            {
                string s = ee.Message;
            }


            
        }
        public async void RedirectOnOTPPage()
        {
            await App.Current.MainPage.Navigation.PushAsync(new Views.OTPVerificationPage());
        }
    }
}
