﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Paizano.Common.NotifyProperty;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
    public class BaseViewModel :NotifyPropertiesChanged
    {
        public BaseViewModel()
        {

        }

        private bool isRunningTasks;
        /// <summary>
        /// /Get or set the IsRunningTasks
        /// </summary>
        public bool IsRunningTasks
        {
            get { return isRunningTasks; }
            set { isRunningTasks = value; OnPropertyChanged(); }
        }

        private string pageHeaderText;

        public string PageHeaderText
        {
            get { return pageHeaderText; }
            set { pageHeaderText = value; OnPropertyChanged(); }
        }

        private string pagesubHeaderText;

        public string PageSubHeaderText
        {
            get { return pagesubHeaderText; }
            set { pagesubHeaderText = value; OnPropertyChanged(); }
        }

        public Action OnPageNavigation;
        public Action<object> OnConditionNavigation;

    }
}
