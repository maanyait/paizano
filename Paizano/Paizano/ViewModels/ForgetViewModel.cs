﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Models;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
  public class ForgetViewModel:BaseViewModel
    {
        public ForgetViewModel()
        {
            ForgetClick = new Command(ExecuteonForget);
        }


        #region Property

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value;OnPropertyChanged("Email"); }
        }

        #endregion

        #region Delegate Command

        public Command ForgetClick { get; set; }
        #endregion


        private async void ExecuteonForget()
        {
            try
            {
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_Forgetpassword, string.Empty);
                var emailjson= "{\"Email\":" + "\"" + Email + "\"}";
                var response = await apicall.Post<ResponseMessage>(emailjson);
                if(response!=null)
                {
                    if(response?.status!=null && response.status=="true")
                    {
                        await App.Current.MainPage.DisplayAlert("Message", response.data, "OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.LoginPage());
                    }
                }else
                {

                    await App.Current.MainPage.DisplayAlert("Message", response.data, "OK");

                }
            }
            catch
            {
                await App.Current.MainPage.DisplayAlert("Error","Somethins is wrong. Please try later","OK");
                
            }
        }



    }
}
