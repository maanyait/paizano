﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
   public class MyTransacrionPageViewModel:BaseViewModel
    {
        public MyTransacrionPageViewModel()
        {
           
            OnPagepreparation();
            ViewData = new Command<LedgerDetail>(ExecuteOnViewData);
        }

        #region  Property

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        private string mobileno;
        public string MobileNo
        {
            get { return mobileno; }
            set { mobileno = value; OnPropertyChanged("MobileNo"); }
        }

        private string photo;
        public string Photo
        {
            get { return photo; }
            set { photo = value; OnPropertyChanged("Photo"); }
        }

        private List<LedgerDetail> _LZList;
        public List<LedgerDetail> LZList
        {
            get { return _LZList; }
            set { _LZList = value; OnPropertyChanged("LZList"); }
        }

        #endregion


        #region Command

        public Command<LedgerDetail> ViewData { get; set; }

        #endregion
        public async void OnPagepreparation()
        {
            LZList = await GetLedgerDetail();
            if (LZList.Count > 0)
            {
                Name = "Name: " + LZList[0].Name;
                MobileNo = "Mobile No.: " + LZList[0].MobileNo;
                Photo = LZList[0].Photo;
            }
        }

        public async Task<List<LedgerDetail>> GetLedgerDetail()
        {
            var ledgerlist = new List<LedgerDetail>();
            try
            {
                FavoriteRequest objinfo = new FavoriteRequest();
                objinfo.MobileNo ="";
                objinfo.CurrentMobile = Settings.PZN_Mobile;
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_GetLedgerDetail, Settings.AccessTokenSettings);
                string json = JsonConvert.SerializeObject(objinfo);
                var response = await apicall.Post<UserTransactionResponse>(json);
                if (response != null)
                {
                    if (response?.status != null && response.status == "true")
                    {
                        int i = 1;
                        foreach (var item in response.data)
                        {
                            LedgerDetail objdata = new LedgerDetail();
                            if (item.ToMobile == Settings.PZN_Mobile)
                            {
                                objdata.SendType = "Receive from " +item.FromMobile;
                            }
                            else
                            {
                                objdata.SendType = "Send to "+ item.ToMobile;
                            }

                            objdata.Type = item.Type;
                            objdata.MobileNo = Settings.PZN_Mobile; ;
                            objdata.Name = Settings.PZN_DisplayName;
                            objdata.Photo = ApiUrls.BaseUrlWeb + Settings.PZN_Piclink;
                            objdata.SNo = i.ToString();
                            objdata.Amount = string.Format("{0:0.00}", Convert.ToDouble(item.Amount));
                            objdata.Date = item.Date;
                            objdata.Description = item.Description;
                           
                            ledgerlist.Add(objdata);
                            i++;
                        }
                    }
                }

            }
            catch
            {

            }
            return ledgerlist;
        }

        public async void ExecuteOnViewData(LedgerDetail item)
        {
           
            try
            {
                await App.Current.MainPage.DisplayActionSheet("Transaction Detail", "Cancel", null, "Transaction: " + item.SendType, "Type:" + item.Type, "Amount: " + item.Amount, "Date: " + item.Date, "Description: " + item.Description);
            }
            catch 
            {

            }
        }

    }





}
