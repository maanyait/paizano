﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;
using Plugin.Contacts;
using Plugin.Contacts.Abstractions;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
  public  class FavContactDetailViewModel :BaseViewModel
    {
        public FavContactDetailViewModel()
        {

            PageHeaderText = "Favorite Contact Details";
            PageSubHeaderText = "";
            OnPagePreparation();
        }
        #region 

        private ObservableCollection<PzContact> _LZList;
        public ObservableCollection<PzContact> LZList
        {
            get { return _LZList; }
            set { _LZList = value; OnPropertyChanged("LZList"); }
        }

        public List<PzContact> _pzcontact;
        public List<PzContact> PZcontacts
        {
            get { return _pzcontact; }
            set { _pzcontact = value; OnPropertyChanged("PZcontacts"); }
        }
        private PzContact _selectedcontact;
        public PzContact SelectedContact
        {
            get { return _selectedcontact; }
            set
            {
                _selectedcontact = value;
                GoToSeletedContact();
            }
        }


        #endregion



        public async void OnPagePreparation()
        {
            LZList = await GetLedgerList();
        }

        public async Task<ObservableCollection<PzContact>> GetLedgerList()
        {
            UserDialogs.Instance.ShowLoading("");
            ObservableCollection<PzContact> PZcontactslist = null;
            var ledgerlist = new ObservableCollection<LedgerModel>();
            try
            {
                int i = 1;
                List<Contact> contacts = null;

                if (await CrossContacts.Current.RequestPermission())
                {

                    CrossContacts.Current.PreferContactAggregation = false;//recommended
                    await Task.Run(() => //run in background
                    {

                        if (CrossContacts.Current.Contacts == null)
                            return;
                        contacts = CrossContacts.Current.Contacts.ToList();
                        contacts = contacts.Where(cont => !string.IsNullOrWhiteSpace(cont.LastName) && cont.Phones.Count > 0).OrderBy(ol => ol.DisplayName).ToList();
                        PZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(JsonConvert.SerializeObject(contacts));

                    });

                }


                FavoriteRequest objinfo = new FavoriteRequest();
                objinfo.MobileNo = Settings.PZN_Mobile;
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_GetAllFavorite, Settings.AccessTokenSettings);
                string json = JsonConvert.SerializeObject(objinfo);
                var response = await apicall.Post<LedgerResponse>(json);
                if (response != null)
                {
                    if (response?.status != null && response.status == "true")
                    {
                        foreach (var item in response.data)
                        {

                            LedgerModel objresult = new LedgerModel();
                            objresult.Id = i.ToString();
                            List<Phone> phones = new List<Phone>();
                            Phone objphone = new Phone();
                            objphone.Number = item.MobileNo;
                            phones.Add(objphone);
                            objresult.Phones = phones;
                            if (PZcontacts.Count > 0)
                            {
                                foreach (var value in PZcontacts)
                                {
                                    string mobile = value.MobileNums.Replace("(", "").Replace(")", "").Replace("-", "").Replace("+", "").Replace(" ", "");

                                    if (mobile == item.MobileNo)
                                    {

                                        objresult.DisplayName = value.FirstName + " " + value.LastName;
                                        break;
                                    }
                                    else
                                    {
                                        objresult.DisplayName = "N/A";
                                    }
                                }
                            }else
                            {
                                objresult.DisplayName = "N/A";

                            }
                            
                            ledgerlist.Add(objresult);
                            PZcontactslist = JsonConvert.DeserializeObject<ObservableCollection<PzContact>>(JsonConvert.SerializeObject(ledgerlist));

                            i++;
                        }
                    }
                }
                UserDialogs.Instance.HideLoading();
                return PZcontactslist;

            }
            catch
            {
                UserDialogs.Instance.HideLoading();
                return PZcontactslist;
            }
        }
        private async void GoToSeletedContact()
        {
            if (SelectedContact == null)
            {
                return;
            }

            await App.Current.MainPage.Navigation.PushAsync(new Views.SendMoneyPage(SelectedContact));
        }
    }
}

