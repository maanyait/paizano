﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paizano.Models;
using Acr.UserDialogs;
using Paizano.Common.Constants;
using Newtonsoft.Json;
using Paizano.Helpers;

namespace Paizano.ViewModels
{
  public  class RegistrationViewModel:BaseViewModel
    {
        public RegistrationViewModel()
        {
            onpagepreparation();
        }

        private ObservableCollection<SecurityQuestion> _securityquslist = new ObservableCollection<SecurityQuestion>();
        public ObservableCollection<SecurityQuestion> Securityquslist
        {
            get { return _securityquslist; }
            set { _securityquslist = value;OnPropertyChanged("Securityquslist"); }
        }


        private async void onpagepreparation()
        {
            Securityquslist = await GetSecurityQuestions(Settings.PZN_DeviceID);
        }

        private async Task<ObservableCollection<SecurityQuestion>> GetSecurityQuestions(string device)
        {
            try
            {
                UserDialogs.Instance.Loading("Please Wait...");
                var listsecurityquestions = new ObservableCollection<SecurityQuestion>();
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Get_securityquestuionlist, string.Empty);
                UserDevice usrdev = new UserDevice();
                usrdev.DeviceID = device;
                var squestionsjson = JsonConvert.SerializeObject(usrdev);

                var response = await apicall.Post<SecurityQuestionDetailResponse>(squestionsjson);
                if (response?.status != null && response.status == "true")
                {
                    foreach (var question in response.data)
                    {

                        listsecurityquestions.Add(new SecurityQuestion
                        {
                            ID = question.ID,
                            Question = question.Question,
                            CountryID = question.CountryID

                        });
                    }
                }
                UserDialogs.Instance.Loading().Hide();
                return listsecurityquestions;
                
            }
            catch 
            {

                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert("Something is wrong...", "Error", "Ok");
                return null;
            }
        }



        public async void ExecuteOnSave(UserAndSecurity user)
        {
            try
            {
                UserDialogs.Instance.Loading("Please Wait...");
              
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.CreateUser, string.Empty);
                var userjson = JsonConvert.SerializeObject(user);
                var response = await apicall.Post<UserAndSecurityResponse>(userjson);
                if (response?.status != null && response.status == "true")
                {
                    if (response.Message == "success")
                    {
                        UserDialogs.Instance.Loading().Hide();
                        Onsuccessfull();
                    }else
                    {
                        UserDialogs.Instance.Loading().Hide();
                        UserDialogs.Instance.Alert(response.data, "Message", "Ok");
                    }
                }else
                {
                    UserDialogs.Instance.Loading().Hide();
                    UserDialogs.Instance.Alert(response.data, "Message", "Ok");
                }

            }
            catch
            {

                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert("Something is wrong...", "Error", "Ok");
              
            }
        }
        private async void Onsuccessfull()
        {
            await App.Current.MainPage.Navigation.PushAsync(new Views.LoginPage());
        }

    }
}
