﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Contacts;
using Plugin.Contacts.Abstractions;

namespace Paizano.ViewModels
{
    public class SendMoneyViewModel : BaseViewModel
    {
        private INavigation _navigation;
        public SendMoneyViewModel(INavigation navigation, PzContact selectedcontact)
        {
            this._navigation = navigation;
            OnPagePrepration(selectedcontact);
            SelectedContact = selectedcontact;
            SendCommand = new Command(ExecuteOnSend);
            CancelCommand = new Command(ExecuteOnCancel);
        }

        #region Proerties
        private PzContact _selectedcontact;
        public PzContact SelectedContact
        {
            get { return _selectedcontact; }
            set { _selectedcontact = value; }
        }

        private ObservableCollection<TransactionType> _transtype = new ObservableCollection<TransactionType>();
        public ObservableCollection<TransactionType> TransTypeList
        {
            get { return _transtype; }
            set { _transtype = value; OnPropertyChanged("TransTypeList"); }
        }
        private TransactionType _selectedtype = new TransactionType();
        public TransactionType SelectedType
        {
            get { return _selectedtype; }
            set { _selectedtype = value; OnPropertyChanged("SelectedType"); }
        }

        private string statusMessage = string.Empty;
        public string StatusMessage
        {
            get { return statusMessage; }
            set { statusMessage = value; OnPropertyChanged(); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged(); }
        }

        private double _amount;
        public double Amount
        {
            get { return _amount; }
            set { _amount = value; OnPropertyChanged(); }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }

        public List<PzContact> _pzcontact;
        public List<PzContact> PZcontacts
        {
            get { return _pzcontact; }
            set { _pzcontact = value; OnPropertyChanged("PZcontacts"); }
        }

        public List<PzContact> _temppzcontact;
        public List<PzContact> TempPZcontacts
        {
            get { return _temppzcontact; }
            set { _temppzcontact = value; OnPropertyChanged("TempPZcontacts"); }
        }
        #endregion
        #region Commands
        public Command SendCommand { get; set; }
        public Command CancelCommand { get; set; }
        #endregion

        #region Command Executions
        private async void OnPagePrepration(PzContact selectedcontact)
        {
            TransTypeList = await GetTransactionType(selectedcontact);
        }
        public async void ExecuteOnSend()
        {
            try
            {
                UserDialogs.Instance.Loading("please wait..");
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_CreateTransaction, Settings.AccessTokenSettings);
                TransactionModel trnsobj = new TransactionModel();
                trnsobj.Addedby = Convert.ToInt32(Settings.PZN_UserID);
                trnsobj.FromUser = Convert.ToInt32(Settings.PZN_UserID);
                trnsobj.RequestType = "New";
                var mob = SelectedContact.MobileNums.Replace("(", "").Replace(")", "").Replace("-", "").Replace("+", "").Replace(" ", "");
                if (mob.Length > 10)
                {
                    mob = mob.Substring(mob.Length - 10);
                }
                trnsobj.ToMobile = mob;
                trnsobj.TransactionTypeID = SelectedType.TransactionTypeID;
                trnsobj.Amount = Amount;
                trnsobj.Description = Description;
                var strjson = JsonConvert.SerializeObject(trnsobj);
                var response = await apicall.Post<TransactionResponse>(strjson);
                if (response != null)
                {
                    if (response.data != null && response.Message == "Success")
                    {
                        //success
                       
                        StatusMessage = "Money send successfully.";
                        Description = "";
                        Amount = 0;


                        CreateFavContact(SelectedContact.MobileNums);
                        await App.Current.MainPage.DisplayAlert("Status", StatusMessage, "OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.SendHistoryPage());
                    }
                    else
                    {
                        //failure
                       
                        StatusMessage = "Please try again";
                        await App.Current.MainPage.DisplayAlert("Status", StatusMessage, "OK");
                    }
                }
                UserDialogs.Instance.Loading().Hide();
            }
            catch 
            {
                UserDialogs.Instance.Loading().Hide();
            }
        }
        public async void ExecuteOnCancel()
        {
            await _navigation.PopAsync();
        }

        public async void CreateFavContact(string mob)
        {
            try
            {
                if (!await PzFileSystem.IsFileExistAsync("favcontacts.txt"))
                {
                    if (await CrossContacts.Current.RequestPermission())
                    {
                        List<Contact> contacts = null;
                        CrossContacts.Current.PreferContactAggregation = false;//recommended
                        await Task.Run(() => //run in background
                        {
                            UserDialogs.Instance.ShowLoading("");
                            if (CrossContacts.Current.Contacts == null)
                                return;
                            contacts = CrossContacts.Current.Contacts.ToList();
                            contacts = contacts.Where(cont => !string.IsNullOrWhiteSpace(cont.LastName) && cont.Phones.Count > 0).OrderBy(ol => ol.DisplayName).ToList();
                            PZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(JsonConvert.SerializeObject(contacts));
                            PZcontacts = PZcontacts.Where(cont => (cont.MobileNums.Contains(mob))).ToList();

                            UserDialogs.Instance.HideLoading();
                        });
                        await PzFileSystem.CreateFile("favcontacts.txt");
                        await PzFileSystem.WriteTextAllAsync("favcontacts.txt", JsonConvert.SerializeObject(PZcontacts));
                    }
                }
                else
                {
                    List<Contact> tempcontacts = null;
                    var contacts = await PzFileSystem.ReadAllTextAsync("favcontacts.txt");
                    PZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(contacts);
                    tempcontacts = CrossContacts.Current.Contacts.ToList();
                    tempcontacts = tempcontacts.Where(cont => !string.IsNullOrWhiteSpace(cont.LastName) && cont.Phones.Count > 0).OrderBy(ol => ol.DisplayName).ToList();
                    TempPZcontacts = JsonConvert.DeserializeObject<List<PzContact>>(JsonConvert.SerializeObject(tempcontacts));
                    TempPZcontacts = TempPZcontacts.Where(cont => (cont.MobileNums.Contains(mob))).ToList();
                    int i = PZcontacts.Where(x => x.MobileNums.Contains(mob)).ToList().Count;
                    if (i < 1)
                    {
                        foreach (var item in TempPZcontacts)
                        {
                            PZcontacts.Add(item);
                        }
                        await PzFileSystem.WriteTextAllAsync("favcontacts.txt", JsonConvert.SerializeObject(PZcontacts));

                    }

                }
            }
            catch
            {

            }
        }

      
        private async Task<ObservableCollection<TransactionType>> GetTransactionType(PzContact selectedcontact)
        {
            try
            {
                var Type_list = new ObservableCollection<TransactionType>();
                HttpClientHelper apicallcheck = new HttpClientHelper(ApiUrls.Url_Checkuserexist, Settings.AccessTokenSettings);
                var mob = selectedcontact.MobileNums.Replace("(", "").Replace(")", "").Replace("-", "").Replace("+", "").Replace(" ", "");
                if (mob.Length > 10)
                {
                    mob = mob.Substring(mob.Length - 10);
                }
                var usenamejson= "{\"UserName\":" + "\"" + mob + "\"}";
                var responsedetail = await apicallcheck.Post<ResponseMessage>(usenamejson);
                if (responsedetail != null)
                {
                    if (responsedetail?.status != null && responsedetail.status == "false" && responsedetail.Message=="Success")
                    {

                        Status = "User not install this app.";
                    }
                }
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.GetTransCategory, Settings.AccessTokenSettings);
                var response = await apicall.Post<TransactionTypeResponse>(string.Empty);
                if (response != null)
                {
                    if (response.data != null)
                    {
                        foreach (var typdetail in response.data)
                        {
                            TransactionType type = new TransactionType();
                            type.TransactionTypeID = typdetail.TransactionTypeID;
                            type.TransactionTypeName = typdetail.TransactionTypeName;
                            Type_list.Add(type);
                        }
                    }
                }
                UserDialogs.Instance.HideLoading();
                return Type_list;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(ex.Message, "Error", "Ok");
                return null;
            }
        }
        #endregion
    }
}
