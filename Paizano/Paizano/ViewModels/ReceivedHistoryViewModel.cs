﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
    public class ReceivedHistoryViewModel : BaseViewModel
    {
        public ReceivedHistoryViewModel()
        {
            FilterHistoryCommand = new Command<string>(OnPagePrepration);
            ItemTapped = new Command<TransactionModel>(OnItemTapped);
            OnPagePrepration("4");
        }
        #region Properties


        private ObservableCollection<TransactionModel> _pzhistory = new ObservableCollection<TransactionModel>();
        public ObservableCollection<TransactionModel> PZHistory
        {
            get { return _pzhistory; }
            set { _pzhistory = value; OnPropertyChanged("PZHistory"); }
        }
        #endregion
        #region Commands
        public Command<string> FilterHistoryCommand { get; set; }
        public Command<TransactionModel> ItemTapped { get; set; }
        #endregion

        private async void OnPagePrepration(string parma)
        {
            PZHistory = await LoadTransactionHistory(parma);
        }
        private async Task<ObservableCollection<TransactionModel>> LoadTransactionHistory(string flag)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("please wait..");
                var contacts = await PzFileSystem.ReadAllTextAsync("pzcontacts.txt");
                var tempcontacts = JsonConvert.DeserializeObject<List<PzContact>>(contacts);
                var list = new ObservableCollection<TransactionModel>();
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_ReceivedHistory, Settings.AccessTokenSettings );
                string jsonstr = "{\"UserId\":" + "" + Settings.PZN_UserID + "" + ",\"flag\":" + "" + flag + "" + "}";
                var response = await apicall.Post<TransHistoryResponse>(jsonstr);
                if (response != null)
                {
                    if (response.data != null)
                    {
                        foreach (var trans in response.data)
                        {
                            TransactionModel trns = new TransactionModel();
                            trns.ID = trans.ID;
                            trns.TransactionTypeID = trans.TransactionTypeID;
                            trns.TransactionTypeName = trans.TransactionTypeName;
                            trns.FromUser = trans.FromUser;
                            trns.ToMobile = trans.ToMobile;
                            var cont_usr = tempcontacts.Where(cont => cont.MobileNums.Replace("(", "").Replace(")", "").Replace("-", "").Replace("+", "").Replace(" ", "").Contains(trans.ToMobile.ToString())).FirstOrDefault();
                            trns.ToUserDisplayName = cont_usr == null ? "N/A" : cont_usr.DisplayName;
                            trns.Amount = trans.Amount;
                            trns.CreatedDate = trans.CreatedDate;
                            trns.Description = trans.Description;
                            trns.RequestType = trans.RequestType;
                            trns.Addedby = trans.Addedby;
                            trns.Isinitiated = trans.Isinitiated;
                            trns.IsAccepted = trans.IsAccepted;
                            trns.AddedDate = trans.AddedDate;
                            string status = "";
                           if(trns.IsAccepted=="0" && trns.Isinitiated=="1")
                            {
                                status = "Click to Accept";
                                trns.Status = "Pending";
                                trns.Color = "Yellow";
                            }else if (trns.IsAccepted == "0" && trns.Isinitiated == "0")
                            {
                                trns.Status = "Rejected";
                                trns.Color = "Red";
                            }else if (trns.IsAccepted == "1")
                            {
                                trns.Status = "Accepted";
                                trns.Color = "Green";
                            }
                                trns.ShowData = trans.Amount.ToString() + ". INR sent by " + trns.ToUserDisplayName + " (" + trns.ToMobile + ") on "+trns.AddedDate +" for "+trns.TransactionTypeName+". ("+trans.Description+"?)"+status;
                            
                            list.Add(trns);
                        }
                    }
                }
                UserDialogs.Instance.HideLoading();
                return list;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Loading().Hide();
                UserDialogs.Instance.Alert(ex.Message, "Error", "Ok");
                return null;
            }
        }


        public async void OnItemTapped(TransactionModel trns)
        {
            try
            {
                var answer = await App.Current.MainPage.DisplayActionSheet("Are you sure want to Accept? ", "Cancel", null, "Accept", "Reject");
                string status = "0", initiate = "0";
                if (answer == "Accept")
                {
                    initiate = "1";
                    status = "1";
                }
                else if (answer == "Reject")
                {
                    initiate = "1";
                    status = "0";
                }
                else
                {
                    initiate = "0";
                    status = "0";
                }
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_AcceptRejectTransaction, Settings.AccessTokenSettings);
                string jsonstr = "{\"ID\":" + "" + trns.ID + "" + ",\"Isinitiated\":" + "" + initiate + "" + ",\"IsAccepted\":" + "" + status + "" + "}";
                var response = await apicall.Post<TransResponse>(jsonstr);
                if (response != null)
                {
                    if (response.Status != null && response.Status == "true")
                    {
                        await App.Current.MainPage.Navigation.PushAsync(new Views.ReceivedHistoryPage());
                    }
                }
            }
            catch { }

        }
    }
}
