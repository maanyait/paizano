﻿using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paizano.Models;
using Paizano.Views;
using Paizano.Helpers;
using Acr.UserDialogs;

using Paizano.Common.Constants;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Paizano.ViewModels
{
    public class MenuViewModel : BaseViewModel
    {
        public MenuViewModel()
        {
            Piclink = ApiUrls.BaseUrlWeb+ Settings.PZN_Piclink;
            DisplayName = Settings.PZN_DisplayName+",";
            OnPagepreparation();
            
            

        }

        private List<Paizano.Models.Menu> menulist = new List<Paizano.Models.Menu>();
        public List<Paizano.Models.Menu> MenuList
        {
            get { return menulist; }
            set { menulist = value; OnPropertyChanged("MenuList"); }
        }

        private Paizano.Models.Menu _selectedmenu;
        public Paizano.Models.Menu SelectedMenu
        {
            get { return _selectedmenu; }
            set
            {
                _selectedmenu = value;
                GoToSeletedMenu();
            }
        }

       

        private string piclink;
        public string Piclink
        {
            get { return piclink; }
            set { piclink = value; }
        }

        private string displayname;
        public string DisplayName
        {
            get { return displayname; }
            set { displayname = value; }
        }
        private string displaymobile;
        public string DisplayMobile
        {
            get { return displaymobile; }
            set { displaymobile = value; }
        }

       

        public async void OnPagepreparation()
        {
            MenuList = await GetMenuList();
            Piclink = ApiUrls.BaseUrlWeb+Settings.PZN_Piclink;
            DisplayName = Settings.PZN_DisplayName + ",";
            DisplayMobile = Settings.PZN_Mobile;
        }

        public async Task<List<Paizano.Models.Menu>> GetMenuList()
        {
            var menulistitem = new List<Paizano.Models.Menu>();
            try
            {
                UserDialogs.Instance.Loading("Please Wait...");
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Dashboard", TargetType = typeof(MenuNavigationPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "My Fav Contacts", TargetType = typeof(FavContactPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Contacts", TargetType = typeof(ContactPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Sent Money", TargetType = typeof(SendHistoryPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Receive Money", TargetType = typeof(ReceivedHistoryPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Ledger", TargetType = typeof(LedgerPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Transactions", TargetType = typeof(MyTransactionPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Profile", TargetType = typeof(ProfileUpdatePage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Password", TargetType = typeof(ChangePasswordPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Security Question", TargetType = typeof(ChangeSecurityPage) });
                menulistitem.Add(new Paizano.Models.Menu() { icon = "", Title = "Logout", TargetType = typeof(Button) });

                UserDialogs.Instance.Loading().Hide();
                
            }
            catch
            {
                UserDialogs.Instance.Loading().Hide();
                return null;
            }

            return menulistitem;
        }

        public async void GoToSeletedMenu()
        {
            try
            {
                if (SelectedMenu == null)
                {
                    return;
                }
                Cache.MasterPage.IsPresented = false;
                if (SelectedMenu.Title == "Logout")
                {
                    ExecuteonLogout();
                }
                else
                {
                    await App.Current.MainPage.Navigation.PushAsync((Page)Activator.CreateInstance(SelectedMenu.TargetType));
                }
            }
            catch { }
        }


      

        private async void ExecuteonLogout()
        {
            Settings.PZN_UserID = "";
            Settings.PZN_Password = "";
            Settings.PZN_Piclink = "";
            await App.Current.MainPage.Navigation.PushAsync(new Views.LoginPage());
        }

    }
}
