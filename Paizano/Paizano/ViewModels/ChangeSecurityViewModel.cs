﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Paizano.Common.Constants;
using Paizano.Helpers;
using Paizano.Models;
using Xamarin.Forms;

namespace Paizano.ViewModels
{
   public class ChangeSecurityViewModel:BaseViewModel
    {
        public ChangeSecurityViewModel()
        {
          
            Onpagepreparation();
            SaveQuestion = new Command(ExecuteOnSave);
        }


        private ObservableCollection<SecurityQuestion> _squestion = new ObservableCollection<SecurityQuestion>();
        public ObservableCollection<SecurityQuestion> Questions
        {
            get { return _squestion; }
            set { _squestion = value; OnPropertyChanged("Questions"); }
        }

        private string answer;
        public string Answer
        {
            get { return answer; }
            set { answer = value; OnPropertyChanged("Answer"); }
        }

        private string questionid;
        public string QuestionID
        {
            get { return questionid; }
            set { questionid = value; OnPropertyChanged("QuestionID"); }
        }

        public Command SaveQuestion { get; set; }

        private async void Onpagepreparation()
        {
            Questions = await GetSecurityQuestions();
        }

        public async Task<ObservableCollection<SecurityQuestion>> GetSecurityQuestions()
        {
            ObservableCollection<SecurityQuestion> Listquestions = new ObservableCollection<SecurityQuestion>();
            try
            {
                UserDialogs.Instance.Loading("Please wait...");
                Models.Device device = new Models.Device();
                device.DeviceID = Settings.PZN_DeviceID;
                
                string json = JsonConvert.SerializeObject(device);
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_GetSecurityquestions, Settings.AccessTokenSettings);
                var response = await apicall.Post<SecurityQuestionDetailResponse>(json);
                if (response != null)
                {
                    if (response?.status != null && response.status == "true")
                    {
                        foreach (var item in response.data)
                        {
                            SecurityQuestion question = new SecurityQuestion();
                            question.ID = item.ID;
                            question.Question = item.Question;
                            question.CountryID = item.CountryID;
                            Listquestions.Add(question);
                        }
                    }
                }
                UserDialogs.Instance.Loading().Hide();

            }
            catch
            {
                UserDialogs.Instance.Loading().Hide();
            }
            return Listquestions;
        }



        public async void ExecuteOnSave()
        {

            try
            {
                UserAndSecurity objinfo = new UserAndSecurity();
                objinfo.flag = 2;
                objinfo.Answer = Answer;
                objinfo.QuestionID =Convert.ToInt32(QuestionID);
                objinfo.UserID =Convert.ToInt32(Settings.PZN_UserID);

                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_UpdatePasswordAndSecurity, Settings.AccessTokenSettings);
                string json = JsonConvert.SerializeObject(objinfo);
                var response = await apicall.Post<UserAndSecurityResponse>(json);
                if(response!=null)
                {
                    if(response?.status!=null && response.status=="true")
                    {
                        await App.Current.MainPage.DisplayAlert("Message","Data Updated Successfully.","OK");
                        await App.Current.MainPage.Navigation.PushAsync(new Views.MenuNavigationPage());
                    }else
                    {
                        await App.Current.MainPage.DisplayAlert("Message", "Data not Updated.", "OK");
                    }
                }
                
            }
            catch (Exception ee)
            {
                await App.Current.MainPage.DisplayAlert("Message", ee.Message, "OK");
            }
        }
    }
}
