﻿using Paizano.Helpers;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using Paizano.ViewModels;
using Paizano.Models;
using Paizano.Common.Constants;
using Newtonsoft.Json;

namespace Paizano
{
    public partial class App : Application
    {
       

        public App()
        {
            InitializeComponent();
            if (string.IsNullOrEmpty(Settings.PZN_DeviceID))
            {
                Settings.PZN_DeviceID = CrossDeviceInfo.Current.GenerateAppId(true, "paizano", "mit");
            }
            if (!string.IsNullOrWhiteSpace(Settings.PZN_UserID)&& !string.IsNullOrWhiteSpace(Settings.PZN_Password))
            {
                MainPage = new NavigationPage(new Views.MenuNavigationPage());
            }
            else
            {
                MainPage = new NavigationPage(new Views.LoginPage());
            }
           // OneSignal.Current.StartInit("a97162b4-64ba-49c9-98e1-503f041bac79").EndInit();
           // GetOnsignalID();



        }

       
        protected override void OnStart()
        {
          //  OneSignal.Current.RegisterForPushNotifications();
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
